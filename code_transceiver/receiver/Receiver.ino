//RECIEVER CODE

//  4 Channel Receiver | 4 Kanal Alıcı
//  PWM output on pins D2, D3, D4, D5 (Çıkış pinleri)
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Servo.h>
int ch_width_1 = 0;
int ch_width_2 = 0;
int ch_width_3 = 0;

Servo ch1;
Servo ch2;
Servo ch3;

struct Signal {
byte throttle;      
byte pitch;
byte roll;
byte yaw;
};

Signal data;
const uint64_t pipeIn = 0xE9E8F0F0E1LL;
RF24 radio(9, 10); 
void ResetData()
{
// Define the inicial value of each data input. | Veri girişlerinin başlangıç değerleri
// The middle position for Potenciometers. (254/2=127) | Potansiyometreler için orta konum
data.throttle = 127; // Motor Stop | Motor Kapalı
data.pitch = 127;  // Center | Merkez
data.roll = 127;   // Center | Merkez
data.yaw = 127;   // Center | Merkez
}
void setup()
{

  Serial.begin(9600);
  Serial.println("hello");
  //Set the pins for each PWM signal | Her bir PWM sinyal için pinler belirleniyor.
  ch1.attach(2);
  ch2.attach(3);
  ch3.attach(4);
  pinMode(5,OUTPUT);
  pinMode(6,OUTPUT);
  //Configure the NRF24 module
  ResetData();
  radio.begin();
  radio.openReadingPipe(1,pipeIn);
  
  radio.startListening(); //start the radio comunication for receiver | Alıcı olarak sinyal iletişimi başlatılıyor
}
unsigned long lastRecvTime = 0;
void recvData()
{
while ( radio.available() ) {
radio.read(&data, sizeof(Signal));
lastRecvTime = millis();   // receive the data | data alınıyor
}
}

void motorStop(int pin1,int pin2){
  digitalWrite(pin1, LOW);
  digitalWrite(pin2, LOW);
}

void motorForw(){
  digitalWrite(5, HIGH);
   digitalWrite(3, LOW);
  ch2.writeMicroseconds(ch_width_2);
}

void motorForwB(){
  digitalWrite(6, HIGH);
   digitalWrite(4, LOW);
  ch3.writeMicroseconds(ch_width_3);
}

void motorBack(){
   digitalWrite(5, LOW);
   digitalWrite(3, HIGH);
  ch2.writeMicroseconds(ch_width_2);
}


void motorBackB(){
   digitalWrite(4, LOW);
   digitalWrite(6, HIGH);
  ch3.writeMicroseconds(ch_width_3);
}


void loop()
{
Serial.println(data.yaw);
//Serial.println(data.roll);
recvData();
unsigned long now = millis();

if ( now - lastRecvTime > 1000 ) {
ResetData(); // Signal lost.. Reset data | Sinyal kayıpsa data resetleniyor
}

ch_width_1 = map(data.throttle, 0, 255, 1000, 2000);     // pin D2 (PWM signal)
ch_width_2 = map(data.pitch,    0, 255, 1000, 2000);     // pin D3 (PWM signal)
ch_width_3 = map(data.roll,     0, 255, 1000, 2000);     // pin D4 (PWM signal)
// Write the PWM signal | PWM sinyaller çıkışlara gönderiliyor

ch1.writeMicroseconds(ch_width_1);
//ch2.writeMicroseconds(ch_width_2);
//ch3.writeMicroseconds(ch_width_3);

////////////////////////////////////////////////////////////////////////
//Motor A
if (data.pitch >145){
  Serial.println(data.pitch);
  Serial.println("Motor A: Forward");
  motorForw();
}
else if (data.pitch<105){
  Serial.println(data.pitch);
  Serial.println("Motor A: Backwards");
  motorBack();
}
else{
  Serial.println(data.pitch);
  Serial.println("Motor A: Stop");
  motorStop(5,3);
}

/////////////////////////////////////////////////////////////////////////
//Motor B
if (data.roll >145){
  Serial.println(data.roll);
  Serial.println("Motor B: Forward");
  motorForwB();
}
else if (data.roll<105){
  Serial.println(data.roll);
  Serial.println("Motor B: Backwards");
  motorBackB();
}
else{
  Serial.println(data.roll);
  Serial.println("Motor B: Stop");
  motorStop(4,6);
}
/////////////////////////////////////////////////////////////////////////
}
