#include <RH_ASK.h>
#include <SPI.h>
#include <PWMServo.h>

//Making servo channel
int servodata = 0;

//PWMServo ch1;
RH_ASK rf_driver;

/////////////////////////create the data object
struct Signal {
  short throttleF;
  short throttleB;
  short rudder;
  short pump;
  bool valve;
};

typedef struct Signal Data;
Data data;

//////////////////////////Set all the data to base values
void ResetData(){
  data.throttleF = 0;
  data.throttleB = 0;
  data.rudder = 127;
  data.pump = 127;
  data.valve = false;
}



void setup() {
  Serial.begin(9600);
 // ch1.attach(2);
  analogWrite(2, 1500);
  pinMode(3,OUTPUT);
  pinMode(4,OUTPUT);

  ResetData();
  rf_driver.init();
}

//////////////////////////////motor control functions
void motorStop(){
  //Serial.println("stop");
  //Serial.println(data.throttleF);
  analogWrite(3, 0);
  analogWrite(4, 0);
}

void motorForw(short throttle){
  //Serial.println("forward");
  //Serial.println(throttle);
  analogWrite(3, 0);
  analogWrite(4, throttle);
}

void motorBack(short throttle){
  //Serial.println("back");
  //Serial.println(throttle);
  analogWrite(3, throttle);
  analogWrite(4, 0);
}

//////////////////////loop function to recieve and handle data
void loop() {
  // put your main code here, to run repeatedly:
  uint8_t buf[sizeof(data)];  
  uint8_t buflen = sizeof(data);
 
  if (rf_driver.recv(buf, &buflen)) {
    memcpy(&data, &buf, buflen);
    
    /*Serial.println(data.throttle);
    Serial.println(data.rudder); 
    Serial.println(data.pump);
    Serial.println(data.valve); */

    //control the throttle through pin D3 and D4
    if (data.throttleF >5){
      motorForw(data.throttleF);
      }
    else if (data.throttleB>5){
      motorBack(data.throttleB);
      }
    else{
      motorStop();
      }

    //control the valve through D7
    if (data.valve){
      digitalWrite(7, HIGH);
      Serial.println("valve on");
      Serial.println(data.valve);
    }
    else {
      digitalWrite(7, LOW);
      Serial.println("valve off");
      Serial.println(data.valve);
    }
    
    //control the servo though D2
    servodata = map(data.rudder, 0,180,1000,2000);
    Serial.println(servodata);
    analogWrite(2, data.rudder);
    
  }
}
